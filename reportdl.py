#!/usr/bin/env python3
import pathlib
import time
import json

import requests
import bs4

import config


SESSION_START_URL = "https://www2.land-oberoesterreich.gv.at/internetfoerderbericht/Start.jsp"
SITEMAP_URL = "https://www2.land-oberoesterreich.gv.at/internetfoerderbericht/InternetFoerderbericht_Start.jsp?jahre={}&cmdSitemap=Sitemap"
BASE_URL = "https://www2.land-oberoesterreich.gv.at/internetfoerderbericht/"
PDF_URL = "https://www2.land-oberoesterreich.gv.at/internetfoerderbericht/InternetFoerderberichtZfi_f_int4SuchenListe.jsp?cmdPDF=PDF"
PRINT_URL = "https://www2.land-oberoesterreich.gv.at/internetfoerderbericht/Druck.jsp"
DISALLOWED_CHARS = "<>:\"/\\|?*"
UA = "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko"


for year in config.YEARS:
    print("Year", year)
    sess = requests.Session()
    sess.headers = {"User-Agent": UA}
    sess.get(SESSION_START_URL).text
    sitemap_html = sess.get(SITEMAP_URL.format(year)).text
    sitemap_soup = bs4.BeautifulSoup(sitemap_html, "html.parser")

    tbl = sitemap_soup.find("table")
    sitemap_entries = []
    for row in tbl.find_all("tr"):
        cols = row.find_all("td")
        entry = {}
        entry["num"] = cols[0].string.strip()
        entry["name"] = cols[1].a.string
        entry["link"] = BASE_URL + cols[1].a["href"]
        docname = "{} {}".format(entry["num"], entry["name"])
        for c in DISALLOWED_CHARS:
            docname = docname.replace(c, "_")
        entry["docname"] = docname
        sitemap_entries.append(entry)

    year_dir = config.STORE_PATH / str(year)
    year_pdf = year_dir / "pdf"
    year_print = year_dir / "print"
    for d in (year_dir, year_pdf, year_print):
        d.mkdir(parents=True, exist_ok=True)

    with open(year_dir / "sitemap.json", "w") as sitemapf:
        json.dump(sitemap_entries, sitemapf, indent=2, sort_keys=True, ensure_ascii=False)

    for entry in sitemap_entries:
        if len(entry["num"]) < 5:
            continue
        print(entry["docname"])
        sess.get(entry["link"]).content
        pdf_req = sess.get(PDF_URL)
        with open(year_pdf / (entry["docname"] + ".pdf"), "wb") as f:
            f.write(pdf_req.content)
        print_req = sess.get(PRINT_URL)
        with open(year_print / (entry["docname"] + ".html"), "w") as f:
            f.write(print_req.text)
        time.sleep(1)

