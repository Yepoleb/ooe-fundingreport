# ooe-fundingreport

Download and process Upper Austrian funding reports.

## Usage

* Copy `config-example.py` to `config.py`
* Edit `config.py`
* Run `reportdl.py` to download PDFs and printable HTML pages
* Create summary PDFs with `pdfunite 2019/pdf/*.pdf 'Förderbericht 2019.pdf'` (requires poppler-utils)
* Run `reportparse.py` to parse printable pages into JSON
* Run `reportsummary.py` to generate summary CSV

## Dependencies

* Python >=3.5
* BeautifulSoup4 (deb: python3-bs4)
* Requests (deb: python3-requests)
* lxml (deb: python3-lxml) for further processing, html.parser can't handle the broken printable HTML ;)

## License

MIT
