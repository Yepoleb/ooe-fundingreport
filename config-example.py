import pathlib

# Where all data is stored
STORE_PATH = pathlib.Path("förderbericht_store")
# Years to process
YEARS = list(range(2006, 2020))
