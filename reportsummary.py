#!/usr/bin/env python3
import json
from collections import defaultdict
from decimal import Decimal
from pprint import pprint
import pathlib
import csv

import config


def parse_amount(amt_str):
    amt_eng = amt_str.replace(".", "").replace(",", ".")
    return Decimal(amt_eng)

def format_amount(amt_dec):
    amt_str = str(amt_dec).replace(".", ",")
    return amt_str

categories = {}
sections = {}
funding = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: {})))
section_sum = defaultdict(lambda: defaultdict(lambda: {}))

for year in config.YEARS:
    year_dir = config.STORE_PATH / str(year)
    sitemap = json.load(open(year_dir / "sitemap.json"))
    for sitemap_cat in sitemap:
        cat_num = sitemap_cat["num"]
        categories[cat_num] = sitemap_cat["name"]
        if len(cat_num) < 5:
            continue
        json_path = year_dir / "json" / (sitemap_cat["docname"] + ".json")
        cat_data = json.load(open(json_path))
        for section in cat_data:
            ident = section["ident"]
            sections[ident] = section["title"]
            section_sum[ident][year] = section["sum"]
            for entry in section["entries"]:
                entry_id = (entry["recipient"], entry["postcode"], entry["loc"])
                funding[cat_num][ident][entry_id][year] = entry["amount"]

summaryf = open(config.STORE_PATH / "summary.csv", "w")
summ_writer = csv.writer(summaryf)

headings_year = [str(x) for x in config.YEARS]
filler_year = [""] * len(config.YEARS)
summ_writer.writerow(["Empfänger", "PLZ", "Ort", "Gesamt"] + headings_year)

def sort_entries(item):
    return sum(parse_amount(x) for x in item[1].values())

for cat_num, cat_name in sorted(categories.items()):
    cat_str = "{} - {}".format(cat_num, cat_name)
    summ_writer.writerow([cat_str, "", "", ""] + filler_year)
    for ident in funding[cat_num].keys():
        section_str = "{} {}".format(ident, sections[ident])
        section_overall = format_amount(sum(parse_amount(section_sum[ident].get(y, "0")) for y in config.YEARS))
        summ_writer.writerow(
            [section_str, "", "", section_overall] +
            [section_sum[ident].get(y, "") for y in config.YEARS])
        for entry_id, yeardata in sorted(funding[cat_num][ident].items(), key=sort_entries, reverse=True):
            entry_overall = format_amount(sum(parse_amount(yeardata.get(y, "0")) for y in config.YEARS))
            summ_writer.writerow(list(entry_id) + [entry_overall] + [yeardata.get(y, "") for y in config.YEARS])
        summ_writer.writerow(["", "", "", ""] + filler_year)

summaryf.close()
