#!/usr/bin/env python3
import json
import pathlib

import bs4

import config


for year in config.YEARS:
    print("Year", year)
    year_dir = config.STORE_PATH / str(year)
    year_print = year_dir / "print"
    year_json = year_dir / "json"
    year_json.mkdir(exist_ok=True)

    for print_path in year_print.iterdir():
        json_path = year_json / (print_path.stem + ".json")

        with open(print_path, "r") as f:
            cat_html = f.read()

        cat_soup = bs4.BeautifulSoup(cat_html, "lxml")
        table = cat_soup.find("table")

        section = None
        sections = []

        for row in table.find_all("tr"):
            strs = list(row.stripped_strings)
            if not strs:
                continue
            bgcolor = row.get("bgcolor")
            if bgcolor == "#B0C4DE":
                if section is None:
                    section = {
                        "title": strs[0],
                        "ident": strs[1],
                        "entries": [],
                        "sum": None
                    }
                else:
                    if section.get("referee"):
                        raise Exception("Referee already set")
                    referee_line = strs[0]
                    if referee_line.startswith("Referent:"):
                        referee = referee_line[len("Referent:"):].strip()
                    else:
                        raise Exception("Unhandled referee: " + repr(referee_line))
                    section["referee"] = referee
            else:
                if section is None:
                    continue

                if bgcolor == "#EFEFEF" or bgcolor == "#F0F8FF":
                    cols = row.find_all("td")
                    entry = {}
                    description = list(cols[0].stripped_strings)
                    if len(description) == 1:
                        entry["recipient"] = description[0]
                    else:
                        entry["recipient"] = description[0]
                        entry["cause"] = ", ".join(description[1:])
                    entry["postcode"] = cols[1].get_text().strip()
                    entry["loc"] = cols[2].get_text().strip()
                    entry["amount"] = cols[3].get_text().strip()
                    section["entries"].append(entry)
                elif bgcolor is None:
                    section["sum"] = strs[1]
                    sections.append(section)
                    section = None
                else:
                    raise Exception("Unhandled line: " + repr(strs))

        with open(json_path, "w") as f:
            json.dump(sections, f, indent=2, sort_keys=True, ensure_ascii=False)
